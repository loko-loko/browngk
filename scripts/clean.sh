VOLUMES_TO_KEEP="postgres
app_media"

docker-compose down
docker image prune -f
docker rm $(docker ps -a -q) -f
# docker volume rm $(docker volume ls | grep -vE "$VOLUMES_TO_KEEP" | awk '/local/ {print $2}')
docker volume rm $(docker volume ls -q)
