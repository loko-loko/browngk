from datetime import datetime

from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator

from .forms import NewArticleForm, LoginForm, NewUserForm, NewCommentForm, NewTagForm
from .models import Article, Tag, Category, Comment
from .decorators import owner_or_superuser_required
from .functions import get_slugger, get_tags, get_categories, get_query
from app.perso.models import MyProfile

PAGINATION_NB = 40

# Home/Search View #

class HomeView(ListView):
    model = Article
    template_name = "blog/home.html"
    context_object_name = "docs"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        return Article.objects\
            .filter(published=True)\
            .order_by("last_modified").reverse()[:6]

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        try:
            context["profile"] = MyProfile.objects.latest("last_modified")
        except:
            context["profile"] = None

        if context["docs"]:
            get_categories(context)
        return context


class ArticleSearchListView(ListView):
    model = Article
    context_object_name = "docs"
    template_name = "blog/doc_list.html"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        self.obj = super(ArticleSearchListView, self)\
                   .get_queryset()\
                   .filter(published=True)
        if self.kwargs.get("cat"):
            self.obj = self.obj.filter(category__name=self.kwargs["cat"])
        query = self.request.GET.get("q")
        if query:
            self.obj = get_query(query, self.obj)
        return self.obj.order_by("last_modified").reverse()

    def get_context_data(self, **kwargs):
        context = super(ArticleSearchListView, self).get_context_data(**kwargs)
        get_tags(context, mode="search")
        context["result_cnt"] = len(self.obj)
        get_categories(context, self.kwargs.get("cat", ""))
        return context


@method_decorator(login_required, name="dispatch")
class DraftCopySearchListView(ListView):
    model = Article
    context_object_name = "docs"
    template_name = "blog/draft_copy_list.html"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        self.obj = super(DraftCopySearchListView, self)\
                   .get_queryset()\
                   .filter(published=False)
        query = self.request.GET.get("q")
        if query:
            self.obj = get_query(query, self.obj)
        return self.obj.order_by("last_modified").reverse()

    def get_context_data(self, **kwargs):
        context = super(DraftCopySearchListView, self).get_context_data(**kwargs)
        context["mode"] = "search"
        context["result_cnt"] = len(self.obj)
        return context

# Article Views #

@method_decorator(login_required, name="dispatch")
class DraftCopyList(ListView):
    model = Article
    context_object_name = "docs"
    template_name = "blog/draft_copy_list.html"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        query = Article.objects\
            .filter(published=False)\
            .order_by("last_modified").reverse()
        if not self.request.user.is_superuser:
            query = query.filter(author=self.request.user)
        return query


class ArticleView(DetailView):
    model = Article
    context_object_name = "doc"
    template_name = "blog/doc_view.html"

    def get_context_data(self, **kwargs):
        context = super(ArticleView, self).get_context_data(**kwargs)
        # Get Comment Form #
        context["form"] = NewCommentForm
        # Get Comments #
        context["comments"] = Comment.objects\
            .filter(article__id=self.kwargs["pk"])\
            .order_by("last_modified")
        comment_childs = [c for c in context["comments"] if c.parent]
        for comment in context["comments"]:
            if not comment.parent:
                comment.childs = [c for c in comment_childs if c.parent == comment]
        # Get Last Articles with Same Tags #
        tags = Article.objects.get(id=self.kwargs["pk"]).tag.all()
        obj = self.get_object()
        obj.count = obj.count + 1
        obj.save()
        context["last_docs_by_tag"] = Article.objects\
            .filter(published=True)\
            .filter(tag__in=tags)\
            .exclude(id=self.kwargs["pk"])\
            .order_by("last_modified").distinct().reverse()[:6]
        get_tags(context)
        if not obj.published:
            if (obj.author.username != self.request.user.username and
              not self.request.user.is_superuser):
                raise Http404
        return context


class ArticleList(ListView):
    model = Article
    context_object_name = "docs"
    template_name = "blog/doc_list.html"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        return Article.objects\
            .filter(published=True)\
            .order_by("last_modified").reverse()

    def get_context_data(self, **kwargs):
        context = super(ArticleList, self).get_context_data(**kwargs)
        get_categories(context)
        get_tags(context)
        return context


class ArticleByCategoryList(ListView):
    model = Article
    context_object_name = "docs"
    template_name = "blog/doc_list.html"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        return Article.objects\
            .filter(published=True)\
            .filter(category__name=self.kwargs["cat"])\
            .order_by("last_modified").reverse()

    def get_context_data(self, **kwargs):
        context = super(ArticleByCategoryList, self).get_context_data(**kwargs)
        get_categories(context, name=self.kwargs["cat"])
        get_tags(context, mode="category")
        return context


class ArticleByTagList(ListView):
    model = Article
    context_object_name = "docs"
    template_name = "blog/doc_list.html"
    paginate_by = PAGINATION_NB

    def get_queryset(self):
        return Article.objects\
            .filter(published=True)\
            .filter(tag__name__iexact=self.kwargs["tag"])\
            .order_by("last_modified").reverse()

    def get_context_data(self, **kwargs):
        context = super(ArticleByTagList, self).get_context_data(**kwargs)
        get_categories(context)
        get_tags(context, mode="tag", name=self.kwargs["tag"])
        return context


@method_decorator(login_required, name="dispatch")
class ArticleCreate(CreateView):
    template_name = "blog/doc_create.html"
    form_class = NewArticleForm

    def form_valid(self, form):
        form.instance.slug = get_slugger(form.instance.title)
        form.instance.category = Category.objects.get(name=form.instance.category)
        form.instance.author = self.request.user
        form.instance.author_update = "{0}.{1}".format(
            self.request.user.first_name.upper()[0],
            self.request.user.last_name.capitalize(),
        )
        self.object = form.save()
        return super(ArticleCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "doc_view",    
            kwargs={"pk": self.object.id, "slug": self.object.slug}
        )


@method_decorator(login_required, name="dispatch")
@method_decorator(owner_or_superuser_required(Article), name="dispatch")
class ArticleUpdate(UpdateView):
    model = Article
    template_name = "blog/doc_modify.html"
    form_class = NewArticleForm

    def form_valid(self, form):
        form.instance.slug = get_slugger(form.instance.title)
        form.instance.category = Category.objects.get(name=form.instance.category)
        form.instance.author_update = "{0}.{1}".format(
            self.request.user.first_name.upper()[0],
            self.request.user.last_name.capitalize(),
        )
        form.instance.last_modified = datetime.now()
        self.id = self.kwargs.get("pk")
        self.slug = form.instance.slug
        return super(ArticleUpdate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "doc_view",
            kwargs={"pk": self.id, "slug": self.slug}
        )


@method_decorator(login_required, name="dispatch")
@method_decorator(owner_or_superuser_required(Article), name="dispatch")
class ArticleDelete(DeleteView):
    model = Article
    context_object_name = "doc"
    template_name = "blog/doc_delete.html"

    def get_success_url(self):
        return reverse_lazy("doc_list")

# Comment Views #

@method_decorator(login_required, name="dispatch")
class CommentCreate(CreateView):
    template_name = "blog/doc_view.html"
    form_class = NewCommentForm

    def form_valid(self, form):
        self.doc = Article.objects.get(id=self.kwargs["did"])
        form.instance.author = self.request.user
        form.instance.article = self.doc
        self.object = form.save()
        if self.kwargs.get("pk"):
            form.instance.parent = Comment.objects.get(id=self.kwargs["pk"])
        return super(CommentCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "doc_view",
            kwargs={"pk": self.doc.id, "slug": self.doc.slug}
        )


@method_decorator(login_required, name="dispatch")
@method_decorator(owner_or_superuser_required(Comment), name="dispatch")
class CommentUpdate(UpdateView):
    model = Comment
    template_name = "blog/doc_view.html"
    form_class = NewCommentForm

    def form_valid(self, form):
        form.instance.last_modified = datetime.now()
        return super(CommentUpdate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "doc_view",
            kwargs={"pk": self.kwargs.get("did"), "slug": self.kwargs.get("dslug")}
        )


@method_decorator(login_required, name="dispatch")
@method_decorator(owner_or_superuser_required(Comment), name="dispatch")
class CommentDelete(DeleteView):
    model = Comment
    context_object_name = "comment"
    template_name = "blog/doc_view.html"

    def get_success_url(self):
        return reverse_lazy(
            "doc_view",
            kwargs={"pk": self.kwargs.get("did"), "slug": self.kwargs.get("dslug")}
        )

# Tag Views #

@method_decorator(staff_member_required, name="dispatch")
class TagList(ListView):
    model = Tag
    context_object_name = "tags"
    template_name = "blog/tag_list.html"

    def get_context_data(self, **kwargs):
        context = super(TagList, self).get_context_data(**kwargs)
        # Get Tag Form #
        context["form"] = NewTagForm
        return context


@method_decorator(staff_member_required, name="dispatch")
class TagCreate(CreateView):
    template_name = "blog/tag_list.html"
    form_class = NewTagForm

    def get_success_url(self):
        return reverse_lazy("tag_list")


@method_decorator(staff_member_required, name="dispatch")
class TagUpdate(UpdateView):
    model = Tag
    template_name = "blog/tag_list.html"
    form_class = NewTagForm

    def get_success_url(self):
        return reverse_lazy("tag_list")


@method_decorator(staff_member_required, name="dispatch")
class TagDelete(DeleteView):
    model = Tag
    template_name = "blog/tag_list.html"

    def get_success_url(self):
        return reverse_lazy("tag_list")


# User Login Views #

def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                next = request.GET.get("next", "/")
                return HttpResponseRedirect(next)
        else:
            messages.error(request, "Bad User/Password")
            return redirect("login")
    else:
        form = LoginForm()
    return render(request, "blog/login.html", {
        "form": form,
    })


def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")
