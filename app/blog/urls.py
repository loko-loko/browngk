from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.HomeView.as_view(), name="home"),

    url(r"^draft_copy$", views.DraftCopyList.as_view(), name="draft_copy_list"),
    url(r"^draft_copy/search$", views.DraftCopySearchListView.as_view(), name="draft_copy_search_list_view"),

    url(r"^login$", views.user_login, name="login"),
    url(r"^logout$", views.user_logout, name="logout"),

    url(r"^docs$", views.ArticleList.as_view(), name="doc_list"),
    url(r"^docs/search/(?P<cat>\w+)?$", views.ArticleSearchListView.as_view(), name="doc_search_list_view"),
    url(r"^docs/category/(?P<cat>\w+)$", views.ArticleByCategoryList.as_view(), name="doc_by_category"),
    url(r"^docs/tag/(?P<tag>\w+)$", views.ArticleByTagList.as_view(), name="doc_by_tag"),
    url(r"^docs/new$", views.ArticleCreate.as_view(), name="doc_create"),
    url(r"^docs/modify/(?P<pk>\d+)/(?P<slug>[\w-]+)$", views.ArticleUpdate.as_view(), name="doc_modify"),
    url(r"^docs/delete/(?P<pk>\d+)/(?P<slug>[\w-]+)$", views.ArticleDelete.as_view(), name="doc_delete"),
    url(r"^docs/(?P<pk>\d+)/(?P<slug>[\w-]+)$", views.ArticleView.as_view(), name="doc_view"),
    url(
        r"^docs/(?P<did>\d+)/(?P<dslug>[\w-]+)/comment/new(?:/(?P<pk>\d+))?$",
        views.CommentCreate.as_view(), name="comment_create"
    ),
    url(
        r"^docs/(?P<did>\d+)/(?P<dslug>[\w-]+)/comment/modify/(?P<pk>\d+)$",
        views.CommentUpdate.as_view(), name="comment_modify"
    ),
    url(
        r"^docs/(?P<did>\d+)/(?P<dslug>[\w-]+)/comment/delete/(?P<pk>\d+)$",
        views.CommentDelete.as_view(), name="comment_delete"
    ),
    url(r"^docs/tags$", views.TagList.as_view(), name="tag_list"),
    url(r"^docs/tags/create$", views.TagCreate.as_view(), name="tag_create"),
    url(r"^docs/tags/modify/(?P<pk>\d+)$", views.TagUpdate.as_view(), name="tag_modify"),
    url(r"^docs/tags/delete/(?P<pk>\d+)$", views.TagDelete.as_view(), name="tag_delete"),
]
