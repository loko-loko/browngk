from django import forms
from django.forms import widgets
from .models import Article, Tag, Comment

class NewArticleForm(forms.ModelForm):

    class Meta:
        model = Article
        fields = "__all__"
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Title"}),
            "content": forms.Textarea(attrs={"class": "form-control", "rows": "40", "cols": "60"}),
            "summary": forms.Textarea(attrs={"class": "form-control", "rows": "2", "placeholder": "Summary"}),
            "source": forms.Textarea(attrs={"class": "form-control", "rows": "2", "placeholder": "Source"}),
            "category": forms.Select(attrs={"class": "form-control", "title": "Select Category"}),
            "tag": forms.SelectMultiple(attrs={
                "class": "selectpicker form-control",
                "title": "Select Tag(s)",
                "data-style": "btn-default",
                "data-live-search" : "true",
            }),
            "category": forms.Select(attrs={"class": "form-control", "placeholder": "Category"}),
            "pusblished": forms.RadioSelect(attrs={"class": "form-check-input"}),
        }


class NewCommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = "__all__"
        widgets = {
            "content": forms.Textarea(attrs={"class": "form-control", "rows": "5", "placeholder": "Your message ..."})
        }


class NewTagForm(forms.ModelForm):

    class Meta:
        model = Tag
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={
                "class": "form-control form-control-sm",
                "style": "height:33px;min-width:100px;",
                "placeholder": "Name"
            }),
            "icon": forms.TextInput(attrs={
                "class": "form-control form-control-sm",
                "style": "height:33px;min-width:100px;",
                "placeholder": "Icon"
            })
        }


class NewUserForm(forms.Form):
    username = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30, widget=forms.TextInput(attrs={
        "class": "my-input",
        "placeholder": "User",
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        "class": "my-input",
        "placeholder": "Password",
    }))
