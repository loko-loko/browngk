from functools import wraps
from django.http import Http404
from django.shortcuts import get_object_or_404


def owner_or_superuser_required(Model=None):
    def decorator(func):
        @wraps(func)
        def _wrapped_view(request, *args, **kwargs):
            pk = kwargs["pk"]
            model = Model
            obj = get_object_or_404(model, pk=pk)
            is_owner = obj.author.id == request.user.id
            is_superuser = request.user.is_superuser
            if not is_owner and not is_superuser: 
                raise Http404
            return func(request, *args, **kwargs)
        return _wrapped_view
    return decorator
