from django.test import TestCase, Client
from django.urls import reverse

from django.contrib.auth.models import User

from .models import Article, Tag, Category, Comment
from .functions import get_slugger

import random

class UserTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.credentials = {'username': 'UserTest', 'password': 'Password'}
        User.objects.create_user(**cls.credentials)

    def test_user_login_logout(self):
        response = self.client.post(reverse('login'), self.credentials, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_active)
        response = self.client.post(reverse('logout'))
        self.assertEqual(response.status_code, 302)

class ArticleTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.credentials = {'username': 'ArticleTest', 'password': 'Password'}
        cls.cat_lst = ['tuto', 'memo', 'code']
        User.objects.create_user(**cls.credentials)
        for t in range(4):
            Tag.objects.create(name='Tag_{0}'.format(t))
        for c in cls.cat_lst:
            user = User.objects.get(username=cls.credentials['username'])
            Category.objects.create(name=c)
            Article.objects.create(
                title = 'article {0}'.format(c),
                slug = get_slugger('article {0}'.format(c)),
                category = Category.objects.get(name=c),
                author = user,
                author_update = 'User',
                content = 'Article Content Test',
                summary = 'Article Summary Test',
                source = 'http://test.com',
                published = True,
                menu = False,
            )
            article = Article.objects.get(title='article {0}'.format(c))
            article.tag.set(Tag.objects.all())
            Comment.objects.create(
                content = 'Comment Content Test',
                author = user,
                article = article,
            )

    def setUp(self):
        self.doc_lst = Article.objects.all().order_by('last_modified').reverse()

    def test_home(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_search_in_cat(self):
        for a in self.doc_lst:
            response = self.client.get(
                reverse('doc_search_list_view'), 
                data = {'q' : 'SearchNotExist'}
            )
            self.assertEqual(response.status_code, 200)
            response = self.client.get(
                reverse('doc_search_list_view'), 
                data = {'q' : a.title}
            )
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, a.title)

    def test_doc_list_by_cat(self):
        for a in self.doc_lst:
            response = self.client.get(reverse('doc_by_category', kwargs={'cat': a.category.name}))
            self.assertEqual(response.status_code, 200)
            self.assertQuerysetEqual(response.context['docs'], [repr(a)])

    def test_doc_list(self):
        response = self.client.get(reverse('doc_list'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['docs'], [repr(a) for a in self.doc_lst])

    def test_doc_view(self):
        for a in self.doc_lst:
            response = self.client.get(
                reverse('doc_view', kwargs = {
                    'pk': a.id,
                    'slug': a.slug
                })
            )
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, a.title)

    def test_doc_post(self):
        for s_code in [302, 200]:
            if s_code == 200:
                self.client.login(**self.credentials)
            response = self.client.post(
                reverse('doc_create'), 
                data = { 'form': {
                    'title' : 'Article Post Test',
                    'content' : 'Article Content',
                    'summary' : 'Article Summary Test',
                    'source' : 'http://test.com',
                    'menu' : False, 
                }}
            )
            self.assertEqual(response.status_code, s_code)
        self.client.logout()
        
    def test_doc_delete(self):
        a = random.choice(self.doc_lst)
        for s_code in [302, 200]:
            if s_code == 200:
                self.client.login(**self.credentials)
            response = self.client.get(
                reverse('doc_delete', kwargs = {
                    'pk': a.id,
                    'slug': a.slug
                }), 
            )
            self.assertEqual(response.status_code, s_code)
        self.client.logout()

    def test_comment_post(self):
        for s_code in [302, 200]:
            if s_code == 200:
                self.client.login(**self.credentials)
            for a in self.doc_lst:
                response = self.client.post(
                    reverse('comment_create', kwargs = {
                        'did': a.id,
                        'dslug': a.slug
                    }),
                    data = { 'form': {'content': 'Comment Post Test'}}
                )
                self.assertEqual(response.status_code, s_code)
        self.client.logout()

    def test_comment_post_response(self):
        for s_code in [302, 200]:
            if s_code == 200:
                self.client.login(**self.credentials)
            for a in self.doc_lst:
                response = self.client.post(
                    reverse('comment_create', kwargs = {
                        'did': a.id,
                        'dslug': a.slug,
                        'pk': Comment.objects.get(article=a).id
                    }),
                    data = { 'form': {'content': 'Comment Post Test Return'}}
                )
                self.assertEqual(response.status_code, s_code)

        self.client.logout()
