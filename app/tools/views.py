import re
import os
import datetime
from collections import OrderedDict

from django.conf import settings
from django.http import Http404, HttpResponse, HttpResponseForbidden, HttpResponseServerError
from django.shortcuts import render, redirect
from django.views.generic import View, ListView, TemplateView
from django.views.generic.edit import FormMixin
from django.utils.decorators import method_decorator
from django.contrib.admin.views.decorators import staff_member_required

from .models import Flower
from .forms import DockerImagePullForm
from .flower_utils import FlowerUtils, TIMES_ELEMENTS
from .docker_utils import DockerImages
from .commons import get_fs_size
from .docker_tasks import image_save, image_pull


@method_decorator(staff_member_required, name="dispatch")
class FlowerView(ListView):
    model = Flower
    context_object_name = "flowers"
    template_name = "tools/flower_info.html"

    def get_queryset(self):
        obj = Flower.objects.all()
        names = set(f.name for f in obj)
        self.flowers = [obj.filter(name=n).latest("collect_date") for n in names]
        return self.flowers

    def get_context_data(self, **kwargs):
        context = super(FlowerView, self).get_context_data(**kwargs)
        flower = FlowerUtils(context, self.flowers)
        flower.get_last_data()
        return context


@method_decorator(staff_member_required, name="dispatch")
class FlowerByPeriodList(ListView):
    model = Flower
    context_object_name = "flower_lst"
    template_name = "tools/flower_period.html"

    def get_queryset(self):
        period_info = TIMES_ELEMENTS.get(self.kwargs["period"], False)
        if not period_info:
            raise Http404
        period_range = datetime.datetime.now() - datetime.timedelta(hours=period_info["hour"])
        self.flowers = Flower.objects.all().filter(
            collect_date__gt=period_range
        ).order_by("collect_date")
        return self.flowers

    def get_context_data(self, **kwargs):
        context = super(FlowerByPeriodList, self).get_context_data(**kwargs)
        flower = FlowerUtils(context, self.flowers)
        flower.get_period_data(self.kwargs["period"], self.kwargs["gr_lvl"])
        return context


@method_decorator(staff_member_required, name="dispatch")
class DockerImageList(FormMixin, TemplateView):
    template_name = "tools/docker_images.html"
    form_class = DockerImagePullForm

    def get_context_data(self, **kwargs):
        context = super(DockerImageList, self).get_context_data(**kwargs)
        context["images"] = DockerImages.get_list()
        context["fs"] = get_fs_size(settings.DOCKER_MEDIA_PATH)
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        image_name = form.cleaned_data.get("image")
        image_pull.delay(image_name)
        return redirect("docker_images")


@method_decorator(staff_member_required, name="dispatch")
class DockerImageSave(View):

    def get(self, request, sid):
        image_save.delay(sid)
        return redirect("docker_images")


@method_decorator(staff_member_required, name="dispatch")
class DockerImageDownload(View):

    def get(self, request, sid):
        image_file, image_url = DockerImages.get_file_from_sid(sid)
        response = HttpResponse(content_type="text/plain")
        response["Content-Disposition"] = f"inline; filename={os.path.basename(image_file)}"
        if settings.DEBUG:
            with open(image_file, "rb") as f:
                response.write(f.read())
        else:
            response["X-Accel-Redirect"] = image_url
        return response


@method_decorator(staff_member_required, name="dispatch")
class DockerImageFileDelete(View):

    def get(self, request, sid):
        DockerImages.delete_file(sid)
        return redirect("docker_images")


@method_decorator(staff_member_required, name="dispatch")
class DockerImageDelete(View):

    def get(self, request, sid):
        DockerImages.delete(sid)
        return redirect("docker_images")
