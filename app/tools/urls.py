from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r"^flower/view$", views.FlowerView.as_view(), name="flower_view"),
    url(r"^flower/by_period/(?P<period>\w+)/(?P<gr_lvl>\d+[hd])$", views.FlowerByPeriodList.as_view(), name="flower_by_period"),

    url(r"^docker/images$", views.DockerImageList.as_view(), name="docker_images"),
    url(r"^docker/image/download/(?P<sid>\w+)$",
        views.DockerImageDownload.as_view(), name="docker_image_download"),
    url(r"^docker/image/save/(?P<sid>\w+)$",
        views.DockerImageSave.as_view(), name="docker_image_save"),
    url(r"^docker/image/file/delete/(?P<sid>\w+)$",
        views.DockerImageFileDelete.as_view(), name="docker_image_file_delete"),
    url(r"^docker/image/delete/(?P<sid>\w+)$",
        views.DockerImageDelete.as_view(), name="docker_image_delete"),
]
