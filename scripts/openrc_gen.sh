BASEDIR=$(dirname "$0")/..

OPENRC_TEMPLATE=$BASEDIR/config/openrc.example
OPENRC_FINAL=$BASEDIR/openrc

# Create final openrc file
cp $OPENRC_TEMPLATE $OPENRC_FINAL

ENVS_TO_DEFINED=$(awk '/# ?To Defined/ {print $2}' $OPENRC_TEMPLATE | tr -d "=")

for env in $ENVS_TO_DEFINED; do
    new_env_value=$(eval echo \$$env)
    if [[ -z $new_env_value ]]; then
        echo "<!> ERROR: $env not find !"
        exit 1
    fi
    new_env_line="export $env='$new_env_value'"
    sed -ri "s/^export $env=.*/$new_env_line/g" $OPENRC_FINAL
done
