from django.db import models

class MyProfile(models.Model):
    first_name = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    job_title = models.CharField(max_length=80)
    content = models.TextField()
    image = models.ImageField(upload_to="profil/img/", blank=True)
    count = models.IntegerField(default=1, editable=False)
    last_modified = models.DateTimeField()
    
    def __str__(self):
        return '{0} {1}'.format(self.first_name, self.name)
        
class SocialLink(models.Model):
    name = models.CharField(max_length=42)
    url = models.TextField()
    icon = models.CharField(max_length=42, default='')
    
    def __str__(self):
        return self.name
    
class SkillCategory(models.Model):
    name = models.CharField(max_length=42)
    icon = models.CharField(max_length=42, default='fas fa-skull')
    
    def __str__(self):
        return self.name
        
class Skill(models.Model):
    name = models.CharField(max_length=42)
    icon = models.CharField(max_length=42, default='fas fa-skull')
    category = models.ForeignKey(SkillCategory, blank=True, on_delete=models.CASCADE)
    percent = models.IntegerField()
    
    def __str__(self):
        return self.name
        
class Experience(models.Model):
    company = models.CharField(max_length=42)
    job_title = models.CharField(max_length=80)
    content = models.TextField()
    date_in = models.DateTimeField()
    date_out = models.DateTimeField()
    
    def __str__(self):
        return self.job_title
        
        
class Formation(models.Model):
    school = models.CharField(max_length=42)
    diploma = models.CharField(max_length=80)
    certificate = models.BooleanField()
    date_in = models.DateTimeField()
    date_out = models.DateTimeField()
    
    def __str__(self):
        return self.diploma