import os
from dateutil import parser

import docker

from django.conf import settings
from .commons import size_conv

class DockerImages():

    @staticmethod
    def _get_client():
        return docker.from_env()

    @classmethod
    def get_file_path(cls, name, tag):
        name_fmt = f"{name}_{tag}.tar".replace("/", "-")
        return os.path.join(settings.DOCKER_MEDIA_PATH, name_fmt)

    @classmethod
    def get_file_url(cls, name, tag):
        name_fmt = f"{name}_{tag}.tar".replace("/", "-")
        return os.path.join(settings.DOCKER_MEDIA_URL, name_fmt)

    @classmethod
    def get_file_from_sid(cls, sid):
        _, name, tag = cls._get(sid)
        return (
            cls.get_file_path(name, tag),
            cls.get_file_url(name, tag)
        )

    @classmethod
    def get_list(cls):
        result = {}
        _client = cls._get_client()
        images = _client.images.list()
        for image in images:
            if not image.tags:
                continue
            is_save = False
            save_is_done = False
            sid = image.short_id.split(":")[1]
            name, tag = image.tags[0].split(":")
            img_size = image.attrs["Size"]
            image_file = cls.get_file_path(name, tag)
            image_url = cls.get_file_url(name, tag)
            if os.path.exists(image_file):
                is_save = True
                file_size = os.path.getsize(image_file)
                if file_size >= img_size:
                    save_is_done = True
            result[sid] = {
                "name": name,
                "tag": tag,
                "size": size_conv(img_size, dec=0),
                "created": parser.parse(image.attrs["Created"]),
                "file": image_file,
                "url": image_url,
                "is_save": is_save,
                "save_is_done": save_is_done,
            }
        return result

    @classmethod
    def _get(cls, sid):
        _client = cls._get_client()
        image = _client.images.get(sid)
        name, tag = image.tags[0].split(":")
        return image, name, tag

    @classmethod
    def delete_file(cls, sid):
        _, name, tag = cls._get(sid)
        image_file = cls.get_file_path(name, tag)
        if os.path.exists(image_file):
            os.remove(image_file)

    @classmethod
    def delete(cls, sid):
        cls.delete_file(sid)
        _client = cls._get_client()
        _client.images.remove(sid)
