from datetime import datetime


class Profile():

    def __init__(self, context, skills, skill_cats, experiences, training):
        self.context = context
        self._skills = skills
        self._skill_cats = skill_cats
        self._experiences = experiences
        self._training = training

    def format_data(self):
        self.context["skill_cats"] = self._skill_cats
        self.context["skills"] = self._format_skills(self._skills)
        self.context["experiences"] = self._format_experiences(self._experiences)
        self.context["training"] = self._format_training(self._training)

    def _format_training_date_range(self, date_in, date_out):
        month_in = self._format_month(date_in)
        month_out = self._format_month(date_out)
        if date_in.year == date_out.year:
            if date_in.month == date_out.month:
                date = f"{month_out} {date_out.year}"
            else:
                date = date_out.year
        else:
            date = f"{date_in.year} - {date_out.year}"
        return date

    def _format_experience_date_range(self, it, date_in, date_out):
        month_in = self._format_month(date_in)
        month_out = self._format_month(date_out)
        if it == 0:
            date = f"{month_in} {date_in.year} to Now"
        elif date_in.year == date_out.year:
            date = f"{month_in} to {month_out} {date_out.year}"
        else:
            date = f"{month_in} {date_in.year} to {month_out} {date_out.year}"
        return date

    def _format_training(self, training):
        formatted_training = []
        for it, train in enumerate(training):
            # get training icon
            icon = "fa-graduation-cap"
            if train.certificate:
                icon = "fa-certificate"
            # get formatted date
            date = self._format_training_date_range(
                date_in=train.date_in,
                date_out=train.date_out
            )
            formatted_training.append({
                "school": train.school,
                "diploma": train.diploma,
                "icon": icon,
                "date_format": date
            })
        return formatted_training

    def _format_experiences(self, experiences):
        formatted_experiences = []
        for it, exp in enumerate(experiences):
            date_out = exp.date_out
            if it == 0:
                date_out = datetime.now(exp.date_out.tzinfo)
            date = self._format_experience_date_range(
                it=it,
                date_in=exp.date_in,
                date_out=date_out
            )
            time_delta = (date_out - exp.date_in)
            formatted_experiences.append({
                "id": it,
                "company": exp.company,
                "job_title": exp.job_title,
                "contents": exp.content.split("\n"),
                "total_time": self._format_time_delta(time_delta),
                "date_format": date
            })
        return formatted_experiences

    @staticmethod
    def _format_skills(skills):
        formatted_skills = []
        for skill in skills:
            bar_type = "info"
            percent = skill.percent
            if percent >= 70:
                bar_type = "success"
            formatted_skills.append({
                "name": skill.name,
                "icon": skill.icon,
                "cat_name": skill.category.name,
                "prc": percent,
                "bar_type": bar_type,
            })
        return formatted_skills

    @staticmethod
    def _format_time_delta(delta):
        delta_sec = int(delta.total_seconds())
        periods = [
            ("Year", 60*60*24*365),
            ("Month", 60*60*24*30),
        ]
        formatted_delta = []
        for name, seconds in periods:
            is_plural = ""
            if delta_sec > seconds:
                period , delta_sec = divmod(delta_sec, seconds)
                if period > 1:
                    is_plural = "s"
                formatted_delta.append(f"{period} {name}{is_plural}")
        return " and ".join(formatted_delta)

    @staticmethod
    def _format_month(date):
        return date.strftime("%b")
