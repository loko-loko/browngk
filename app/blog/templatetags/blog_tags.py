import re

from django import template
from datetime import datetime
from urllib.parse import urlencode
from django import template
import markdown as md

register = template.Library()

@register.filter
def markdown(value):
    md_exts = [
        'markdown.extensions.extra',
        'markdown.extensions.fenced_code',
        'markdown.extensions.admonition',
        'markdown.extensions.codehilite',
        'markdown.extensions.meta',
        'markdown.extensions.nl2br',
        'markdown.extensions.sane_lists',
        'markdown.extensions.smarty',
        'markdown.extensions.toc',
        'markdown.extensions.wikilinks',
    ]
    return md.markdown(value, extensions=md_exts)


@register.filter
def tag_name_check(value, query_lst):
    for qr in query_lst:
        if qr.name == value:
            return True
    return False


@register.filter()
def t_split(value, sep):
    return value.split(sep)


@register.filter()
def re_search(value, reg):
    if re.search(reg, value):
        return True
    return False


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)
    return urlencode(query)


@register.filter
def convert_datetime_delta(date_time):
    try:
        delta_sec = int((datetime.now() - date_time.replace(tzinfo=None)).total_seconds())
    except:
        delta_sec, delta_fmt, value = (0, 0, '-')
    if delta_sec < 60:
        delta_fmt = delta_sec
        value = 'Second'
    elif delta_sec < 3600:
        delta_fmt = int(delta_sec/60)
        value = 'Minute'
    elif delta_sec < 86400:
        delta_fmt = int(delta_sec/60/60)
        value = 'Hour'
    elif delta_sec < 2592000:
        delta_fmt = int(delta_sec/60/60/24)
        value = 'Day'
    elif delta_sec < 31104000:
        delta_fmt = int(delta_sec/60/60/24/30)
        value = 'Month'
    else:
        delta_fmt = int(delta_sec/60/60/24/30/12)
        value = 'Year'
    if delta_fmt != 1:
        value += 's'
    return '{0} {1}'.format(delta_fmt, value)
