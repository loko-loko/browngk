from django.views.generic import ListView

from .models import MyProfile, SkillCategory, Skill, Experience, Formation
from .profile import Profile

class MyProfileView(ListView):
    model = MyProfile
    context_object_name = "profile"
    template_name = "perso/my_profile_view.html"

    def get_queryset(self):
        obj = MyProfile.objects.latest("last_modified")
        obj.count += 1
        obj.save()
        return obj

    def get_context_data(self, **kwargs):
        context = super(MyProfileView, self).get_context_data(**kwargs)
        skill_cats = SkillCategory.objects.all()
        skills = Skill.objects.all().order_by("percent").reverse()
        experiences = Experience.objects.all().order_by("date_in").reverse()
        training = Formation.objects.all().order_by("date_in").reverse()
        profile = Profile(
            context=context,
            skills=skills,
            skill_cats=skill_cats,
            experiences=experiences,
            training=training
        )
        profile.format_data()
        return context
