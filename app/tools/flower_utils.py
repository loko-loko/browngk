import operator
from collections import OrderedDict
from statistics import mean

from .commons import (
    get_week_day,
    get_rgba_color,
    get_granularity_levels,
    convert_granularity_value
)

METRICS_INFOS = {
    "light": {
        "min": 2700,
        "max": 7800,
        "icon": "sun",
        "color": [255, 206, 86, 1],
    },
    "moisture": {
        "min": 15,
        "max": 60,
        "icon": "swimming-pool",
        "color": [54, 162, 235, 1],
    },
    "temperature": {
        "min": 5,
        "max": 32,
        "icon": "thermometer-quarter",
        "color": [255, 99, 132, 1],
    },
    "conductivity": {
        "min": 100,
        "max": 1500,
        "icon": "spa",
        "color": [75, 192, 192, 1],
    }
}


TIMES_ELEMENTS = {
    "day": {
        "name": "1 Day",
        "name_min": "1D",
        "time_lvl": ("year", "month", "day", "hour"),
        "gr_default": "1h",
        "hour": 24
    },
    "day_7": {
        "name": "7 Days",
        "name_min": "7D",
        "time_lvl": ("year", "month", "day", "hour"),
        "gr_default": "4h",
        "hour": 168
    },
    "month": {
        "name": "1 Month",
        "name_min": "1M",
        "time_lvl": ("year", "month", "day", "hour"),
        "gr_default": "12h",
        "hour": 744
    },
    "month_3": {
        "name": "3 Months",
        "name_min": "3M",
        "time_lvl": ("year", "month", "day"),
        "gr_default": "1d",
        "hour": 2232
    },
    "year": {
        "name": "1 Year",
        "name_min": "1Y",
        "time_lvl": ["year", "month", "day"],
        "gr_default": "5d",
        "hour": 8760
    },
    "year_3": {
        "name": "3 Years",
        "name_min": "3Y",
        "time_lvl": ("year", "month", "day"),
        "gr_default": "10d",
        "hour": 26280
    }
}


MONTH_NAMES = {
    1:"Jan", 2:"Feb", 3:"Mar", 4:"Apr", 5:"May", 6:"Jun", 7:"Jul", 8:"Aug", 9:"Sep", 10:"Oct", 11:"Nov", 12:"Dec"
}


class FlowerUtils():

    def __init__(self, context, flowers):
        self.flowers = flowers
        self.context = context
        self.context["time_elements"] = self._get_time_elements()
        self.metrics = METRICS_INFOS.keys()

    def get_last_data(self):
        for flower in self.flowers:
            flower.data = {}
            for metric in self.metrics:
                flower.data[metric.capitalize] = self._get_attr_state(
                    metric,
                    getattr(flower, metric)
                )
            flower.battery = self._get_battery_state(flower.battery)
        return self.flowers

    def get_period_data(self, period, gr_value):
        # get time elements info
        time_attrs = TIMES_ELEMENTS[period]
        time_types = time_attrs["time_lvl"]
        # add period and title to context
        self.context["period"] = period
        self.context["title"] = time_attrs["name"]
        # get granularity infos
        gr_type, gr_lvl = convert_granularity_value(gr_value)
        self.context["gr_value"] = gr_value
        self.context["gr_default"] = time_attrs["gr_default"]
        self.context["gr_levels"] = get_granularity_levels(gr_type)
        # build dict with metrics info (name, color, icon)
        self.context["metrics_info"] = self._build_metrics_info()
        # get metrics data
        data = self._get_data_metrics(time_types, gr_type, gr_lvl)
        # build metrics data
        times_attrs, max_times_attrs = self._build_data_metrics(data)
        # format metrics
        self._format_data_metrics(times_attrs, max_times_attrs)
        # add metrics time
        self.context["metrics_time"] = self._build_metrics_time(period, max_times_attrs)

    def _get_data_metrics(self, time_types, gr_type, gr_lvl):
        # set to granularity to None if lvl is 1
        if gr_lvl == 1:
            gr_lvl = None
        # declare previous times tuple
        last_times = tuple()
        data = OrderedDict()
        for e, flower in enumerate(self.flowers):
            name = flower.name
            if not data.get(name):
                data[name] = OrderedDict()
            # get only granularity values
            if gr_lvl and getattr(flower.collect_date, gr_type) % gr_lvl != 0:
                if not last_times:
                    continue
                times = last_times
            else:
                # build tuple with date elements
                times = []
                for time_type in time_types:
                    times.append(getattr(flower.collect_date, time_type))
                times = tuple(times)
                # create times and attribute dict if not exist
                if not data[name].get(times):
                    data[name][times] = OrderedDict()
                    for metric in self.metrics:
                        data[name][times][metric] = []
            # append attributes values
            if not data[name].get(times):
                continue
            for metric in self.metrics:
                data[name][times][metric].append(getattr(flower, metric))
            last_times = times
        return data

    def _build_data_metrics(self, data):
        # build flower_data by metric name
        self.context["flower_data"] = OrderedDict()
        for metric in self.metrics:
            self.context["flower_data"][metric] = OrderedDict()
        # declare times and max times attributes
        max_times_attrs = []
        times_attrs = OrderedDict()
        # format data stats
        for e, (name, flower) in enumerate(data.items()):
            values = OrderedDict()
            times_attrs[name] = flower.keys()
            for metric in self.metrics:
                values[metric] = []
            # get stats for values
            for times, dat in flower.items():
                for metric, val in dat.items():
                    values[metric].append(round(mean(val), 2))
            for metric in self.metrics:
                self.context["flower_data"][metric][name] = self._get_stats(
                    e,
                    values[metric],
                    metric
                )
            # set max times attributes
            if len(times_attrs[name]) > len(max_times_attrs):
                max_times_attrs = times_attrs[name]
        return times_attrs, max_times_attrs

    def _format_data_metrics(self, times_attrs, max_times_attrs):
        for flower, attrs in times_attrs.items():
            if attrs == max_times_attrs:
                continue
            for c, attr in enumerate(max_times_attrs):
                if attr in attrs:
                    continue
                # insert 0 if metric is not present
                for metric in self.metrics:
                    self.context["flower_data"][metric][flower]["elements"].insert(c, 0)

    def _build_metrics_info(self):
        infos = {}
        for metric in self.metrics:
            infos[metric] = self._get_metric_info(metric)
        return infos

    @staticmethod
    def _get_time_elements():
        return sorted(
            TIMES_ELEMENTS.items(),
            key=lambda x:operator.getitem(x[-1], "hour")
        )

    @staticmethod
    def _get_attr_state(metric, data):
        attr = dict(**METRICS_INFOS[metric])

        attr["current"] = data
        attr["color"] = get_rgba_color(attr["color"])[0]
        if data < attr["min"]:
            attr["bt_state"] = "danger"
        elif data > attr["max"]:
            attr["bt_state"] = "danger"
        elif data <= attr["min"] or data >= attr["max"]:
            attr["bt_state"] = "warning"
        else:
            attr["bt_state"] = "success"
        attr["percent"] = data * 100 / attr["max"]

        return attr

    @staticmethod
    def _get_stats(e, elements, metric):
        attr = dict(**METRICS_INFOS[metric])

        attr["elements"] = elements
        attr["stat_min"] = int(min(elements))
        attr["stat_max"] = int(max(elements))
        attr["stat_mean"] = int(mean(elements))

        if e is 0:
            new_rgba_lst = attr["color"]
        else:
            new_rgba_lst = [
                (attr["color"][0] - 20),
                (attr["color"][1] + 15),
                (attr["color"][2] - 20),
                1
            ]
        attr["color"], attr["color_shadow"] = get_rgba_color(new_rgba_lst)

        return attr

    @staticmethod
    def _get_battery_state(level):
        bt_state = "success"

        if level > 95:
            icon = "battery-full"
        elif level > 75:
            icon = "battery-three-quarters"
        elif level > 45:
            icon = "battery-half"
        elif level > 15:
            icon = "battery-quarter"
            bt_state = "warning"
        else:
            icon = "battery-empty"
            bt_state = "danger"

        return {
            "name": "Battery",
            "level": level,
            "percent": "{0}%".format(level),
            "bt_state": bt_state,
            "icon": icon,
        }

    @staticmethod
    def _get_metric_info(metric):
        elem_infos = METRICS_INFOS[metric]
        return {
            "name": metric.capitalize(),
            "color": get_rgba_color(elem_infos["color"])[0],
            "icon": elem_infos["icon"]
        }

    @staticmethod
    def _build_metrics_time(period, attrs):
        result = []
        for attr in attrs:
            if period == "day":
                year, month, day, hour = attr
                result.append(f"{hour}h")
            elif period == "day_7":
                year, month, day, hour = attr
                w_day = get_week_day(year, month, day)
                result.append(f"{w_day} {day} ({hour}h)")
            elif period == "month":
                year, month, day, hour = attr
                w_day = get_week_day(year, month, day)
                result.append(f"{w_day} {day} ({hour}h)")
            elif period == "month_3":
                year, month, day = attr
                month_name = MONTH_NAMES[month]
                result.append(f"{day} {month_name}")
            elif period == "year":
                year, month, day = attr
                month_name = MONTH_NAMES[month]
                result.append(f"{day} {month_name}")
            elif period == "year_3":
                year, month, day = attr
                month_name = MONTH_NAMES[month]
                result.append(f"{month_name} {year}")
        return result
