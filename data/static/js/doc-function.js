///////////////////////////
// See More Tag

function more_tag() {
    
    size_li = $(".load_tag a").size();
    x=15;
    
    $('.load_tag a:lt(' + x + ')').show();
    
    $('#load_more').click(function () {
        x = (x + 15 <= size_li) ? x + 15 : size_li;
        $('.load_tag a:lt(' + x + ')').show();
        if (x == size_li) { $('#load_more').hide(); }
    });
    
};

///////////////////////////
// Search Tag

function search_tag() {
    
    $("#tag_search").on("keyup", function() {
        
        var value = $(this).val().toLowerCase();
        
        $('#load_more').hide();
        
        $("#tag_list a").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
};
   

///////////////////////////
// Lighbox Image

function lightbox_img() {
    
    $('#doc-content img').each(function(i) {
        
        var img = $(this);
        var img_href = img.attr('src');
        var div_work = $('<div>').attr('class', 'work');
        var div_over = $('<div>').attr('class', 'overlay');
        var div_cont = $('<div>').attr('class', 'work-content');
        var div_link = $('<div>').attr('class', 'work-link');
        
        div_work.append(div_over);
        div_work.append(div_cont);
        div_cont.append(div_link);
        
        div_link.append(
            $('<a>').attr({
                'class': 'lightbox',
                'href': img_href,
            }).append(
                $('<i>').attr('class', 'fa fa-search')
            )
        );
        
        $(img).replaceWith(div_work);
        
        div_work.prepend(img);
        
    });
}; 
    
///////////////////////////
// href format

function href_fmt(text) {
    new_href = text.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
    new_href = new_href.replace(/[\(\)\[\]\']/g, '');
    new_href = new_href.replace(/[ \/]/g, '-');
    
    return new_href;
};

///////////////////////////
// Doc Menu

function doc_menu (){
    
    var title_lst = $('#doc-content h1, #doc-content h2, #doc-content h3');
    
    var h1_href = '';
    var h2_href = '';
    
    var h2_check = false;
    var h3_check = false;
    
    var h1_cnt = 0;
    var h2_cnt = 0;
    
    var title_max_lenght = 40;
    var sidebar_width = $('div.sidebar-offcanvas').width();
    
    var menu_div = $('<div>');
    
    if (sidebar_width < 180) {
        title_max_lenght = 25;
    } else if (sidebar_width < 220) {
        title_max_lenght = 30;
    }
    
    for (var x = 0; x < title_lst.length; x++) {
        
        var next_oc = x;
        var next_title_cnt = title_lst.length - 1;
        var title = title_lst[x];
        var title_content = title.textContent;
        var title_tag = title.tagName;
        var title_href = href_fmt(title_content);
        
        if (next_oc < next_title_cnt) { next_oc = x + 1; }
        
        var title_next_tag = title_lst[next_oc].tagName;
        
        if (title_content.length >= title_max_lenght) {
            title_content = title_content.substring(0, title_max_lenght) + '..';
        }
        
        var menu_li = $('<li>').css({
            'margin-top': '3px',
        });
        
        if (title_tag == "H1") {
            
            h1_href = title_href;
            h1_cnt = x;
            
            h2_check = false;
            h3_check = false;
            
            css_attribute = {
                'font-size': '15px',
            };
            
            if (title_next_tag == "H2") {
                
                title_icon = $('<a>').attr({
                    'aria-expanded': 'true',
                    'data-toggle': 'collapse',
                    'data-target': '#collap_' + h1_cnt,
                });
                
                title_icon.append($('<i>').attr({
                    'class': 'far fa-plus-square fa-fw',
                    'font-size': '10px',
                }).css({'cursor': 'pointer'}));
                
                title_icon.append($('<i>').attr({
                    'class': 'far fa-minus-square fa-fw',
                    'font-size': '10px',
                }).css({'cursor': 'pointer'}));
                
            } else {
                
                title_icon = $('<i>').attr({
                    'class': 'far fa-square fa-fw',
                    'font-size': '10px',
                });   
            }
            
        } else if (title_tag == "H2") {
            
            h2_href = title_href;
            h2_cnt = x;
            
            if (h2_check == false) {
                h2_div = $('<div>').attr({
                    'id': 'collap_' + h1_cnt,
                    'class': 'collapse show', 
                }); 
            }                
            
            h2_check = true;
            h3_check = false;
            
            title_href = h1_href + '-' + title_href;
            
            css_attribute = {
                'font-size': '14px',
                'margin-left': '17.5px',
            };
            
            if (title_next_tag == "H3") {
                
                title_icon = $('<a>').attr({
                    'aria-expanded': 'false',
                    'data-toggle': 'collapse',
                    'data-target': '#collap_' + h2_cnt,
                });
                
                title_icon.append($('<i>').attr({
                    'class': 'fa fa-caret-right fa-fw',
                }).css({'cursor': 'pointer'}));
                
                title_icon.append($('<i>').attr({
                    'class': 'fa fa-caret-down fa-fw',
                }).css({'cursor': 'pointer'}));
                
            } else {
                
                title_icon = $('<i>').attr({
                    'class': 'fa fa-angle-right fa-fw',
                }); 
            }
            
        } else if (title_tag == "H3") {
            
            title_href = h1_href + '-' + h2_href + '-' + title_href;
            
            if (h3_check == false) {
                h3_div = $('<div>').attr({
                    'id': 'collap_' + h2_cnt,
                    'class': 'collapse', 
                }); 
            }
            
            h2_check = true;
            h3_check = true;
            
            title_icon = $('<i>').attr({
                'class': 'fa fa-angle-right fa-fw',
            });
            
            css_attribute = {
                'font-size': '13.5px',
                'margin-left': '34.5px',
            }; 
        }
        
        $(title).attr('id', title_href);
        
        title_content = $('<a>').text(title_content).attr({
            'data-toggle': 'offcanvas',
            'class': 'nav-item',
            'href': '#' + title_href,
        }).css('color', 'white');
        
        $(menu_li).append(title_content);
        $(menu_li).prepend(' ');
        $(menu_li).prepend(title_icon);
        

        $(menu_li).css(css_attribute);
        
        if (h3_check == true) {
            menu_li = $(h3_div).append(menu_li);
        }
        
        if (h2_check == true) {
            menu_li = $(h2_div).append(menu_li);
        }
        
        $('#menu_list').append(menu_li);
        
    }
    
    $('.expand_all').click(function(){
        $('#menu_list div.collapse').collapse('show');
    });
    $('.hide_all').click(function(){
        $('#menu_list div.collapse').collapse('hide');
    });
    
};

///////////////////////////
// Code Modal

function code_modal (){
    
    $('#doc-content .code_example').each(function(i) {
        
        var div_with_example = $(this);
        var div_code_content = $(this).children('pre').children('code').html();
        
        $(div_with_example).append(
            $('<button>').text(" Afficher le Résultat").attr({
                'class': 'btn btn-outline-success btn-sm',
                'data-target': '.modal_example_' + i,
                'data-toggle': 'modal',
            }).css('font-size', '12px').prepend(
                $('<i>').attr('class', 'fas fa-cubes fa-fw')
            )
        );
        
        var modal_div_1 = $('<div>').attr({
            'aria-hidden': 'true',
            'class': 'modal fade modal_example_' + i,
            'role': 'dialog',
            'tabindex': '-1',
        });
        
        var modal_div_2 = $('<div>').attr({
            'class': 'modal-dialog modal-lg',
        });
        
        var modal_div_3 = $('<div>').attr({
            'class': 'modal-content'
        }).css({
            'padding': '15px',
            'padding-top': '30px',
            'min-height': '400px',
        });
        
        var modal_div_4 = $('<div>').attr({
            'class': 'example_' + i,
        }).html(
            div_code_content.replace(/&nbsp;/g, ' ')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/\$\(\'/g, '$(\'.example_' + i + ' ')
        );
        
        $(div_with_example).append(
            modal_div_1.append(
                modal_div_2.append(
                    modal_div_3.append(modal_div_4)
        )));
    });
};


///////////////////////////
// SVG Transform

function svg_transform (){

    var img_lst = $('#doc-content img');
    
    for (var x = 0; x < img_lst.length; x++) {
        
        $(img_lst[x]).each(function(){
            
            var img = $(this);
            var img_id = img.attr('id');
            var img_class = img.attr('class');
            var img_url = img.attr('src');
            var img_css = img.attr('style');

            $.get(img_url, function(data) {
                
                var svg = $(data).find('svg');

                if(typeof img_id != 'undefined') {
                    svg = svg.attr('id', img_id);
                }
                
                if(typeof img_class != 'undefined') {
                    svg = svg.attr('class', img_class);
                }

                svg = svg.removeAttr('xmlns:a');

                if(! svg.attr('viewBox') && svg.attr('height') && svg.attr('width')) {
                    svg.attr('viewBox', '0 0 ' + svg.attr('height') + ' ' + svg.attr('width'))
                }
                
                img.replaceWith($('<div>').attr('style', img_css).append(svg));

            }, 'xml');

        });
    }
};

///////////////////////////
// Add Icon to Pre

function add_icon_pre (){
    
    new ClipboardJS('.btn_copy');
    
    var div_icon_css = {
        'font-size': '15px',
        'background': 'white',
        'border-radius': '50%',
        'height': '25px',
        'width': '25px',
        'position': 'absolute',
        'margin-top': '-11px',
        'margin-left': '-30px',
    };
    
    var i_icon_css = {
        'position': 'absolute',
        'margin-left': '1px',
        'margin-top': '2px',
    }
    
    $('code').parents('pre').css({
        'border-left': '5px solid #8282b0',
        'background-color': '#f0f0f7',
    });
    
    $('#doc-content pre.error').prepend(
        $('<div>').css(div_icon_css).css('border', '3px solid red').prepend(
            $('<i>').attr('class', 'fas fa-exclamation fa-fw').css(i_icon_css).css({
                'color': 'red',
            })
        )
    );
    $('#doc-content pre.warning').prepend(
        $('<div>').css(div_icon_css).css('border', '3px solid orange').prepend(
            $('<i>').attr('class', 'fas fa-exclamation fa-fw').css(i_icon_css).css({
                'color': 'orange',
            })
        )
    );
    $('#doc-content pre.info').prepend(
        $('<div>').css(div_icon_css).css('border', '3px solid #0066ff').prepend(
            $('<i>').attr('class', 'fas fa-info fa-fw').css(i_icon_css).css({
                'color': '#0066ff',
            })
        )
    );
    
    $('#doc-content pre code').each(function(i){
        $(this).attr('id', 'code_' + i);
        $(this).prepend(
            $('<button>').attr({
                'class': 'btn_copy',
                'data-clipboard-target': '#doc-content pre code#code_' + i,
            }).css({
                'font-size': '16px',
                'background': 'white',
                'border-radius': '50%',
                'height': '28px',
                'width': '28px',
                'position': 'absolute',
                'margin-top': '-11px',
                'margin-left': '-36px',
                'cursor': 'pointer',
                'border': '3px solid #8282b0',
            }).prepend(
                $('<i>').attr('class', 'fas fa-copy').css({
                    'margin-left': '-1px',
                    'color': '#8282b0',
                })
            )
        );
    });
};