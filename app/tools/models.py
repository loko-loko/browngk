from django.db import models

class Flower(models.Model):
    name = models.CharField(max_length=100)
    temperature = models.FloatField()
    moisture = models.IntegerField()
    light = models.IntegerField()
    conductivity = models.IntegerField()
    battery = models.IntegerField()
    collect_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    
    def __str__(self):
        return self.name