from django.contrib import admin
from .models import MyProfile, SkillCategory, Skill, Experience, Formation, SocialLink

admin.site.register(MyProfile)
admin.site.register(SkillCategory)
admin.site.register(Skill)
admin.site.register(Experience)
admin.site.register(Formation)
admin.site.register(SocialLink)
