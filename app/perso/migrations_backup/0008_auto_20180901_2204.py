# Generated by Django 2.0.7 on 2018-09-01 22:04

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('perso', '0007_auto_20180901_2203'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Me',
            new_name='MyProfile',
        ),
        migrations.AlterField(
            model_name='experience',
            name='date_out',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 1, 22, 4, 36, 600647, tzinfo=utc)),
        ),
    ]
