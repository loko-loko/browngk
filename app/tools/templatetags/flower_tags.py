import re

from django import template
from ..commons import convert_granularity_value

register = template.Library()

@register.filter(name="lookup")
def lookup(value, arg):
    return value[arg]


@register.filter(name="gr_format")
def gr_format(value):
    gr_type, gr_lvl = convert_granularity_value(value)
    if gr_lvl != 1:
        gr_type += "s"
    return f"{gr_lvl} {gr_type.capitalize()}"
