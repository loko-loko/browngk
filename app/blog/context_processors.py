from django.conf import settings
from app.perso.models import SocialLink

def get_social_link_lst(request):
    social_link_lst = SocialLink.objects.all().order_by('name')
    return {'social_link_lst': social_link_lst}