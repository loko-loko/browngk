from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import settings

urlpatterns = [
    path(r"admin/", admin.site.urls),
    url("", include("app.blog.urls")),
    url(r"^profile/", include("app.perso.urls")),
    url(r"^tools/", include("app.tools.urls")),
    url(r"^ckeditor/", include("ckeditor_uploader.urls")),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
