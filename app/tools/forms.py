from django import forms
from django.forms import widgets

class DockerImagePullForm(forms.Form):
    image = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'my-input',
                'placeholder': 'Pull New Image',
            }
        )
    )
