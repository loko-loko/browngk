import os
import re

def get_week_day(year, month, day):
    offset = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
    week   = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]

    after_feb = 1
    if month > 2:
        after_feb = 0
    aux = year - 1700 - after_feb
    day_week = 5
    day_week += (aux + after_feb) * 365
    day_week += int(aux / 4 - aux / 100 + (aux + 100) / 400)
    day_week += int(offset[month - 1] + (day - 1))
    day_week %= 7

    return week[day_week]


def get_rgba_color(lst):
    str_lst = [str(r) for r in lst]
    color = "rgba({0})".format(",".join(str_lst))
    str_lst[-1] = "0.2"
    color_shadow = "rgba({0})".format(",".join(str_lst))

    return color, color_shadow


def size_conv(size, value="B", dec=1):
    values = ["B", "K", "M", "G", "T", "P"]
    for c, val in enumerate(values):
        cnext = c + 1
        if value != val:
            continue
        if size > 1024:
            size = size/1024
            if cnext != len(values):
                value = values[cnext]
    if dec:
        size = round(size, dec)
    else:
        size = int(size)
    return "{0}{1}".format(size, value)


def get_icon_with_percent(percent):
    if percent > 90:
        icon = "danger"
    elif percent > 70:
        icon = "warning"
    else:
        icon = "success"
    return icon


def get_granularity_levels(gr_type):
    gr_range = {"hour":24, "day":30}.get(gr_type)
    levels = []
    for g in range(1, gr_range):
        if gr_range % g == 0:
            levels.append(f"{g}{gr_type[0]}")
    return levels


def convert_granularity_value(value):
    re_match = re.match(r"(\d+)([hd])", value)
    gr_lvl = int(re_match.group(1))
    gr_type = {"h": "hour", "d": "day"}.get(
      re_match.group(2)
    )
    return gr_type, gr_lvl


def get_fs_size(fs):
    statvfs = os.statvfs(fs)
    fs_size = statvfs.f_frsize * statvfs.f_blocks
    fs_free = statvfs.f_frsize * statvfs.f_bfree
    fs_use = fs_size - fs_free
    fs_percent = 100 * fs_use / fs_size

    return {
        "percent": round(fs_percent, 1),
        "icon": get_icon_with_percent(fs_percent),
        "size": size_conv(fs_size),
        "used": size_conv(fs_use)
    }
