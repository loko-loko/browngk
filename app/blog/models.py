import time

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from ckeditor_uploader.fields import RichTextUploadingField

class Tag(models.Model):
    name = models.CharField(max_length=42)
    icon = models.CharField(max_length=42, default='')

    def lower_name(self):
        return self.name.lower()

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=42)
    image = models.ImageField(upload_to='image/blog/categories/', blank=True)
    icon = models.CharField(max_length=42, default='')
    name_fmt = models.CharField(max_length=42, default='')
    summary = models.TextField(default='')

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=100)
    slug = models.CharField(max_length=100, editable=False)
    tag = models.ManyToManyField(Tag, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    author = models.ForeignKey(User, blank=True, editable=False, on_delete=models.CASCADE)
    author_update = models.CharField(max_length=100, blank=True, editable=False)
    content = RichTextUploadingField()
    summary = models.TextField(blank=True)
    source = models.TextField(blank=True)
    image = models.ImageField(
        upload_to='image/blog/docs/{0}/'.format(time.strftime("%Y_%m_%d")), 
        blank=True, default='image/blog/docs/default.jpg'
    )
    count = models.IntegerField(default=1, editable=False)
    created = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    last_modified = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    published = models.BooleanField(default=False)
    menu = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Comment(models.Model):
    article = models.ForeignKey(Article, blank=True, editable=False, on_delete=models.CASCADE)
    author = models.ForeignKey(User, blank=True, editable=False, on_delete=models.CASCADE)
    parent = models.ForeignKey('self', null=True, editable=False, blank=True, on_delete=models.CASCADE)
    content = models.TextField(blank=False)
    created = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    last_modified = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    published = models.BooleanField(default=True)

    def __str__(self):
        return '{0} : {1}'.format(self.article.title, self.content)
