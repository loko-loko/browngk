import os
from django.conf import settings

# Global vars
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = os.environ.get("SECRET_KEY", "@secret")
DEBUG = os.environ.get("DEBUG", "0") == "1"
TEST = os.environ.get("TEST", "0") == "1"
ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "localhost").split(",")
# DB vars
DB_HOST = os.environ.get("DB_HOST", "postgres")
DB_PORT = os.environ.get("DB_PORT", "5432")
DB_NAME = os.environ.get("DB_NAME", "django")
DB_USER = os.environ.get("DB_USER", "django")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "password")
REDIS_HOST = os.environ.get("REDIS_HOST", "redis")

settings.configure(
    DEBUG=DEBUG,
    SECRET_KEY=SECRET_KEY,
    ALLOWED_HOSTS=ALLOWED_HOSTS,
)

if not DEBUG:
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    CSRF_COOKIE_SECURE = True

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "ckeditor",
    "ckeditor_uploader",
    "app.blog",
    "app.perso",
    "app.tools",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "BGK_Project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "data/templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "app.blog.context_processors.get_social_link_lst",
            ],
        },
    },
]

WSGI_APPLICATION = "BGK_Project.wsgi.application"

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

if TEST:
    DEFAULT_DB = {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "browngk.db.sqlite3"),
    }

else:
    DEFAULT_DB = {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": DB_NAME,
        "USER": DB_USER,
        "PASSWORD": DB_PASSWORD,
        "HOST": DB_HOST,
        "PORT": DB_PORT,
    }

DATABASES = {"default": DEFAULT_DB}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    { "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator", },
    { "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator", },
    { "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator", },
    { "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator", },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

LOGIN_URL = "/login"
LOGIN_REDIRECT_URL = "home"

MEDIA_ROOT = os.path.join(BASE_DIR, "data/media/")

if DEBUG:
    STATICFILES_DIRS = ( os.path.join(BASE_DIR, "data/static/"), )

else:
    STATIC_ROOT = os.path.join(BASE_DIR, "data/static/")

##  Ckeditor Config ##

CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor/"
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = "pillow"

CKEDITOR_CONFIGS = {
    "default": {
        "height": 500,
        "width": "100%",
        "skin": "bootstrapck",
        "scayt_autoStartup": True,
        "scayt_sLang": "fr_FR",
        "toolbar_Custom": [
            ["Styles","Format", "FontSize"],
            ["Scayt", "Smiley"],
            ["Bold", "Italic", "Underline"],
            [ "TextColor","BGColor" ],
            ["NumberedList", "BulletedList", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-", "Indent"],
            ["Link", "Unlink"],
            ["RemoveFormat", "-", "Source"],
            ["Image", "Table", "-", "HorizontalRule", "-", "SpecialChar" ],
            ["CodeSnippet", "-",  "CreateDiv", "Markdown"]
        ],
        "codeSnippet_theme": "docco",
        "removePlugins": "stylesheetparser",
        "removeButtons": ",".join(["Underline", "Subscript", "Superscript"]),
        "allowedContent": True,
        "toolbar": "Custom",
        "extraPlugins": ",".join(["codesnippet", "markdown"]),
        "stylesSet": [
            {"name": "Keyboard Phrase", "element": "kbd"},
            {"name": "Sample Text", "element": "samp"},
            {"name": "Variable", "element": "var"},
            {"name": "Inserted-Text", "element": "ins"},
            {"name": "Code-Computer", "element": "code", "attributes": {"class": "computer_code"}},
            {"name": "Code-Word", "element": "code", "attributes": {"class": "word_code"}},
            {"name": "Cited-Work", "element": "cite"},
            {"name": "Type-Writer", "element": "tt"},
            {"name": "Container", "element": "tt", "attributes": {"class": "container"}},
            {"name": "Warning", "element": "pre", "attributes": {"class": "warning"}},
            {"name": "Error", "element": "pre", "attributes": {"class": "error"}},
            {"name": "Info", "element": "pre", "attributes": {"class": "info"}},
            {"name": "Terminal", "element": "pre", "attributes": {"class": "terminal"}},
        ],

    },
}

CELERY_BROKER_URL = f"redis://{REDIS_HOST}"
CELERY_BACKEND_URL = f"redis://{REDIS_HOST}"

DOCKER_PATH = "docker/"

DOCKER_MEDIA_PATH = os.path.join(MEDIA_ROOT, DOCKER_PATH)
DOCKER_MEDIA_URL = os.path.join(MEDIA_URL, DOCKER_PATH)
