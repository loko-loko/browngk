import os

import docker
from celery import shared_task

from django.conf import settings
from .docker_utils import DockerImages

@shared_task
def image_save(sid):
    if not os.path.exists(settings.DOCKER_MEDIA_PATH):
        os.mkdir(settings.DOCKER_MEDIA_PATH)
    client = docker.from_env()
    image = client.images.get(sid)
    name, tag = image.tags[0].split(":")
    name_fmt = f"{name}_{tag}.tar".replace("/", "-")
    image_file = os.path.join(settings.DOCKER_MEDIA_PATH, name_fmt)
    # create empty file
    open(image_file, 'wb').close()
    # get image chunks
    chunks = image.save(named=True)
    # write chunks in file
    with open(image_file, "wb") as f:
        for chunk in chunks:
            f.write(chunk)

@shared_task
def image_pull(name):
    client = docker.from_env()
    tag = "latest"
    if ":" in name:
        name, tag = name.split(":")
    client.images.pull(f"{name}:{tag}")
