import operator
from re import sub
from functools import reduce
from unicodedata import normalize

from django.db.models import Q

from .models import Category, Tag


def get_slugger(text, encoding="utf-8"):
    slugger = normalize("NFD", text.lower()).encode("ascii", "ignore").decode(encoding)
    slugger = sub(r"[\(\)\[\]\"]", "", slugger)
    slugger = sub(r"[ \/]", "-", slugger)
    return slugger


def get_tags(context, mode="", name=""):
    context["mode"] = mode
    context["tags"] = Tag.objects.order_by("name").distinct()
    if mode == "tag":
        context["tag_name"] = Tag.objects.get(name__iexact=name).name
    for tag in context["tags"]:
        tag.doc_count = len(tag.article_set.filter(published=True))
    context["tags"] = sorted(
        context["tags"],
        key=lambda k: k.doc_count,
        reverse=True
    )
    return context


def get_categories(context, name=""):
    context["categories"] = Category.objects.all()
    context["category_name"] = name
    if name:
        cat = context["categories"].get(name__iexact=name)
        context["category_icon"] = cat.icon
    for category in context["categories"]:
        category.doc_count = len(category.article_set.filter(published=True))
    return context


def get_query(query, obj):
    queries = query.split()
    obj = obj.filter(
        reduce(operator.and_,
            (Q(title__icontains=q) for q in queries)) |
        reduce(operator.and_,
            (Q(content__icontains=q) for q in queries))
    )
    return obj
